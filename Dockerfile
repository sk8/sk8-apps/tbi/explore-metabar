#v2.0.4
FROM rocker/verse:4.3.3
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y update && apt-get install -y git-core libcurl4-openssl-dev libgit2-dev libglpk-dev libgmp-dev libicu-dev libpng-dev libssl-dev libxml2-dev make pandoc pandoc-citeproc zlib1g-dev libfontconfig1-dev libfribidi-dev openjdk-11-jdk && rm -rf /var/lib/apt/lists/*
RUN apt-get -y update && apt-get install -y curl wget perl make bash xdg-utils && wget https://github.com/marbl/Krona/releases/download/v2.8.1/KronaTools-2.8.1.tar && tar -xvf KronaTools-2.8.1.tar && rm KronaTools-2.8.1.tar && cd KronaTools-2.8.1 && ./install.pl && rm -rf /var/cache/apt/
RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl', Ncpus = 4)" >> /usr/local/lib/R/etc/Rprofile.site
RUN R CMD javareconf
RUN R -e 'install.packages(c("devtools", "remotes"), quiet = TRUE)'
RUN Rscript -e 'remotes::install_version("processx",upgrade="never", version = "3.8.0", quiet = TRUE)'
RUN Rscript -e 'remotes::install_version("rmarkdown",upgrade="never", version = "2.18", quiet = TRUE)'
RUN Rscript -e 'remotes::install_version("knitr",upgrade="never", version = "1.41", quiet = TRUE)'
RUN Rscript -e 'remotes::install_version("pkgload",upgrade="never", version = "1.3.2", quiet = TRUE)'
RUN Rscript -e 'remotes::install_version("testthat",upgrade="never", version = "3.1.5", quiet = TRUE)'
RUN Rscript -e 'install.packages("BiocManager", quiet = TRUE)'
RUN Rscript -e 'BiocManager::install("rhdf5")'
RUN Rscript -e 'BiocManager::install("Biostrings")'
RUN Rscript -e 'BiocManager::install("XVector")'
RUN Rscript -e 'BiocManager::install("Biobase")'
RUN Rscript -e 'BiocManager::install("phyloseq")'
RUN Rscript -e 'BiocManager::install("microbiome")'
RUN Rscript -e 'BiocManager::install("metagenomeSeq")'
RUN Rscript -e 'BiocManager::install("DESeq2")'
RUN Rscript -e 'BiocManager::install("mixOmics")'
RUN Rscript -e 'remotes::install_github("grunwaldlab/metacoder")'
RUN Rscript -e 'remotes::install_github("dreamRs/shinyWidgets")'
RUN Rscript -e 'remotes::install_github("vqf/nVennR")'
RUN Rscript -e 'remotes::install_github("vmikk/metagMisc")'
RUN Rscript -e 'remotes::install_github("mikemc/speedyseq")'

RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone
# Custom cache invalidation
ARG CACHEBUST=1
RUN R -e 'Sys.setenv(GITHUB_PAT="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");remotes::install_gitlab(repo = "umrf/exploremetabar", host = "forgemia.inra.fr", upgrade="never")'
RUN rm -rf /build_zone
EXPOSE 3838

CMD R -e "options('shiny.port'=3838,shiny.host='0.0.0.0',shiny.maxRequestSize=20*1024^2);library(ExploreMetabar);run_app()"
